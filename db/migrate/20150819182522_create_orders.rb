class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string     :client_name
      t.references :seller, index: true, foreign_key: true

      t.decimal    :subtotal, precision: 12, scale: 3
      t.decimal    :total, precision: 12, scale: 3

      t.timestamps null: false
    end
  end
end
