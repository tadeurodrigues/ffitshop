class Order < ActiveRecord::Base
  belongs_to :seller
  has_many   :order_items
end
