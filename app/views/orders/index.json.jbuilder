json.array!(@orders) do |order|
  json.extract! order, :id, :client_name, :Seller_id, :Product_id
  json.url order_url(order, format: :json)
end
