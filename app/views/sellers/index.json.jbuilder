json.array!(@sellers) do |seller|
  json.extract! seller, :id, :name, :registration
  json.url seller_url(seller, format: :json)
end
