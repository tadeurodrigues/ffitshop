require 'rails_helper'

RSpec.describe "orders/show", type: :view do
  before(:each) do
    @order = assign(:order, Order.create!(
      :client_name => "Client Name",
      :Seller => nil,
      :Product => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Client Name/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
