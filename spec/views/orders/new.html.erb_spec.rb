require 'rails_helper'

RSpec.describe "orders/new", type: :view do
  before(:each) do
    assign(:order, Order.new(
      :client_name => "MyString",
      :Seller => nil,
      :Product => nil
    ))
  end

  it "renders new order form" do
    render

    assert_select "form[action=?][method=?]", orders_path, "post" do

      assert_select "input#order_client_name[name=?]", "order[client_name]"

      assert_select "input#order_Seller_id[name=?]", "order[Seller_id]"

      assert_select "input#order_Product_id[name=?]", "order[Product_id]"
    end
  end
end
