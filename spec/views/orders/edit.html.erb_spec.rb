require 'rails_helper'

RSpec.describe "orders/edit", type: :view do
  before(:each) do
    @order = assign(:order, Order.create!(
      :client_name => "MyString",
      :Seller => nil,
      :Product => nil
    ))
  end

  it "renders the edit order form" do
    render

    assert_select "form[action=?][method=?]", order_path(@order), "post" do

      assert_select "input#order_client_name[name=?]", "order[client_name]"

      assert_select "input#order_Seller_id[name=?]", "order[Seller_id]"

      assert_select "input#order_Product_id[name=?]", "order[Product_id]"
    end
  end
end
