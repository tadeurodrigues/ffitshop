require 'rails_helper'

RSpec.describe "sellers/show", type: :view do
  before(:each) do
    @seller = assign(:seller, Seller.create!(
      :name => "Name",
      :registration => "Registration"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Registration/)
  end
end
